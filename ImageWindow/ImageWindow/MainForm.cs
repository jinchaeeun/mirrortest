﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MainForm
{
    static class Program
    {
        /// <summary>
        /// 해당 응용 프로그램의 주 진입점입니다.
        /// </summary>
        [STAThread]

        static void Main()
        {
            /*
             * Application.EnableVisualStyles();
             * Application.SetCompatibleTextRenderingDefault(false);
             * Application.Run(new Form1());
            */

            //파일 이름 변경 함수 실행 코드
            //MessageBox.Show(Rename(@"C:\img\owg", "cc1.png", "0101_1.png"));

            string filePath = @"C:\img\owg";
            string[] oldFile = new string[] { "1.png", "2.png", "3.png", "4.png" };
            for (int i = 0; i <= oldFile.Length; i++)
            {
                FileExistsCheck(filePath, oldFile[i]);
            }

        }


    //파일 존재 여부 체크
    private static bool FileExistsCheck(string filePath, string oldFile)
        {
            oldFile = filePath + "\\" + oldFile;
            if (System.IO.File.Exists(oldFile))
            {
                //MessageBox.Show("File EXISTS");
                return true;
            }
            else
            {
                //MessageBox.Show("File NOT EXISTS");
                return false;
            }

        }

        /* 파일 이름 변경하기
        private static string Rename(string filePath, string oldFile, string newFile)
        {

            oldFile = filePath + "\\" + oldFile;
            newFile = filePath + "\\" + newFile;


            if (FileExistsCheck(oldFile))
            {
                System.IO.File.Move(oldFile, newFile);
                return "FILE NAME CHANGE :: " + oldFile + ">>" + newFile;
            }
            else
            {
                return "FILE NOT EXISTS";
            }
        }
        */
    }
}
