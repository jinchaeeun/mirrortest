﻿namespace ImageWindow
{
    partial class Form1
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.dn = new System.Windows.Forms.ComboBox();
            this.num = new System.Windows.Forms.TextBox();
            this.filename = new System.Windows.Forms.TextBox();
            this.exdate = new System.Windows.Forms.TextBox();
            this.s = new System.Windows.Forms.TextBox();
            this.url = new System.Windows.Forms.TextBox();
            this.price = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.color = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // dn
            // 
            this.dn.FormattingEnabled = true;
            this.dn.Location = new System.Drawing.Point(138, 24);
            this.dn.Name = "dn";
            this.dn.Size = new System.Drawing.Size(121, 20);
            this.dn.TabIndex = 0;
            this.dn.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // num
            // 
            this.num.Location = new System.Drawing.Point(32, 23);
            this.num.Name = "num";
            this.num.Size = new System.Drawing.Size(100, 21);
            this.num.TabIndex = 1;
            this.num.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // filename
            // 
            this.filename.Location = new System.Drawing.Point(265, 24);
            this.filename.Name = "filename";
            this.filename.Size = new System.Drawing.Size(100, 21);
            this.filename.TabIndex = 2;
            this.filename.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // exdate
            // 
            this.exdate.Location = new System.Drawing.Point(371, 23);
            this.exdate.Name = "exdate";
            this.exdate.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.exdate.Size = new System.Drawing.Size(100, 21);
            this.exdate.TabIndex = 3;
            this.exdate.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
            // 
            // s
            // 
            this.s.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.s.Location = new System.Drawing.Point(477, 24);
            this.s.Name = "s";
            this.s.ReadOnly = true;
            this.s.Size = new System.Drawing.Size(100, 21);
            this.s.TabIndex = 4;
            this.s.Text = "s";
            this.s.TextChanged += new System.EventHandler(this.textBox4_TextChanged);
            // 
            // url
            // 
            this.url.Location = new System.Drawing.Point(583, 23);
            this.url.Name = "url";
            this.url.Size = new System.Drawing.Size(100, 21);
            this.url.TabIndex = 5;
            this.url.TextChanged += new System.EventHandler(this.textBox5_TextChanged);
            // 
            // price
            // 
            this.price.Location = new System.Drawing.Point(799, 24);
            this.price.Name = "price";
            this.price.ReadOnly = true;
            this.price.Size = new System.Drawing.Size(100, 21);
            this.price.TabIndex = 6;
            this.price.Text = "12000";
            this.price.TextChanged += new System.EventHandler(this.textBox6_TextChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(915, 22);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 7;
            this.button1.Text = "작업";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // color
            // 
            this.color.Location = new System.Drawing.Point(689, 24);
            this.color.Name = "color";
            this.color.Size = new System.Drawing.Size(100, 21);
            this.color.TabIndex = 8;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1038, 70);
            this.Controls.Add(this.color);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.price);
            this.Controls.Add(this.url);
            this.Controls.Add(this.s);
            this.Controls.Add(this.exdate);
            this.Controls.Add(this.filename);
            this.Controls.Add(this.num);
            this.Controls.Add(this.dn);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Form1";
            this.Text = "ImageWindow";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox dn;
        private System.Windows.Forms.TextBox num;
        private System.Windows.Forms.TextBox filename;
        private System.Windows.Forms.TextBox exdate;
        private System.Windows.Forms.TextBox s;
        private System.Windows.Forms.TextBox url;
        private System.Windows.Forms.TextBox price;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox color;
    }
}

